package ru.vyatsu.blackbox;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;

public class FunctionTable {

    public static List<SimpleEntry<Double, Double>> buildTable(final double a, final double b, final double step,
                                                               final double xMin, final double xMax) {
        if (xMin > xMax) {
            throw new IllegalArgumentException("xMax должно быть не меньше xMin");
        }
        if (step <= 0) {
            throw new IllegalArgumentException("step должен быть положительным");
        }

        final double eps = step / 2;
        final List<SimpleEntry<Double, Double>> table = new ArrayList<>();

        for (double x = xMin; x < xMax + eps; x += step) {
            Double value;
            try {
                value = f(x, a, b);
                if (value.isNaN() || value.isInfinite()) {
                    value = null;
                }
            } catch (Exception e) {
                value = null;
            }
            table.add(new SimpleEntry<>(x, value));
        }

        return table;
    }

    private static double f(final double x, final double a, final double b) {
        return Math.sin((Math.pow(Math.E, x) - 3 * a) / (a * a + b * b)) + 10 / (x * x * x);
    }
}
