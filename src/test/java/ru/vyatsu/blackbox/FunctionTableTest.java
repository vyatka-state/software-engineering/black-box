package ru.vyatsu.blackbox;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.AbstractMap.SimpleEntry;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class FunctionTableTest {

    @Test
    public void testAllPointsValid() {
        final double xMin = 10;
        final double xMax = 20;
        final double step = 1;
        final double a = 1;
        final double b = -2;
        final List<SimpleEntry<Double, Double>> expectedTable = List.of(
            new SimpleEntry<>(10.0, 0.18928401740513062),
            new SimpleEntry<>(11.0, -0.9913377642120962),
            new SimpleEntry<>(12.0, -0.3057403239062892),
            new SimpleEntry<>(13.0, 0.7744530238590785),
            new SimpleEntry<>(14.0, -0.07300630759167027),
            new SimpleEntry<>(15.0, -0.2500849944061613),
            new SimpleEntry<>(16.0, -0.5192371355315615),
            new SimpleEntry<>(17.0, -0.41985371388920817),
            new SimpleEntry<>(18.0, -0.7475669366082254),
            new SimpleEntry<>(19.0, -0.029649185176092465),
            new SimpleEntry<>(20.0, -0.9781377509453892)
        );

        final List<SimpleEntry<Double, Double>> actualTable = FunctionTable.buildTable(a, b, step, xMin, xMax);

        assertEquals(expectedTable, actualTable);
    }

    @ParameterizedTest
    @CsvSource({"20,10", "1.001,1"})
    public void testMaxLessThanMin(final double xMin, final double xMax) {
        final double step = 1;
        final double a = 1;
        final double b = -2;

        assertThrows(IllegalArgumentException.class, () -> FunctionTable.buildTable(a, b, step, xMin, xMax),
            "xMax должно быть не меньше xMin");
    }

    @ParameterizedTest
    @CsvSource({"0", "-15.5"})
    public void testNotPositiveStep(final double step) {
        final double a = 1;
        final double b = -2;
        final double xMin = 10;
        final double xMax = 20;

        assertThrows(IllegalArgumentException.class, () -> FunctionTable.buildTable(a, b, step, xMin, xMax),
            "step должен быть положительным");
    }

    @ParameterizedTest
    @CsvSource({"-10,10", "0,1", "-1,0"})
    public void testPunctureZeroPoint(final double xMin, final double xMax) {
        final double step = 1;
        final double a = 1;
        final double b = -2;

        final List<SimpleEntry<Double, Double>> actualTable = FunctionTable.buildTable(a, b, step, xMin, xMax);
        final Optional<SimpleEntry<Double, Double>> zeroPoint = actualTable.stream()
            .filter(point -> point.getKey() == 0)
            .findAny();

        assertTrue(zeroPoint.isPresent());
        assertNull(zeroPoint.get().getValue());
    }

    @Test
    public void testDivisionByZero() {
        final double xMin = 10;
        final double xMax = 20;
        final double step = 1;
        final double a = 0;
        final double b = 0;

        final List<SimpleEntry<Double, Double>> actualTable = FunctionTable.buildTable(a, b, step, xMin, xMax);

        assertTrue(actualTable.stream()
            .allMatch(point -> point.getValue() == null));
    }

    @ParameterizedTest
    @CsvSource({"1,2,3", "1,1.5,1.55"})
    public void testOnePointInTable(final double xMin, final double xMax, final double step) {
        final double a = 1;
        final double b = -2;

        final List<SimpleEntry<Double, Double>> actualTable = FunctionTable.buildTable(a, b, step, xMin, xMax);

        assertEquals(1, actualTable.size());
        assertEquals(9.943686172425243, actualTable.get(0).getValue());
    }
}